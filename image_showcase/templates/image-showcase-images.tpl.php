<?php
/**
 * @file
 * Display for image listing.
 */
 $images = $data;
?>
<!-- image-zoom starts here-->
<div class="image-showcase-zoom">
  <div id="image-showcase-zoom_id" class="image-showcase-zoom__sliderlg">
    <?php if (!empty($images)): ?>
      <?php foreach ($images as $image): ?>
        <div class="item"><img src="<?php print image_style_url('image_showcase_normal', $image['uri']);?>" data-zoom-image="<?php print image_style_url('image_showcase_zoom', $image['uri']);?>">
        </div>
      <?php endforeach; ?>
    <?php endif; ?>
  </div>
  <?php if (!empty($images)):  ?>
    <div class="image-showcase-zoom__text"><?php print t('Roll over image to zoom in'); ?></div>
  <?php endif; ?>
  <div class="image-showcase-zoom__slider">
  <?php if (!empty($images)): ?>
    <?php
      $attributes = array();
      if(count($images) == 1) {
        $attributes['style'] = 'cursor: default';
      }
    ?>
    <?php foreach ($images as $image): ?>
      <div class="item">
        <img <?php echo drupal_attributes($attributes); ?> src="<?php print image_style_url('image_showcase_thumbnail', $image['uri']); ?>" alt="<?php print t('Image'); ?>" />
      </div>
    <?php endforeach; ?>
  <?php endif; ?>
  </div>
</div>
<!-- image-zoom ends here-->
<div id="preload">
  <?php if (!empty($images)): ?>
    <?php foreach ($images as $image): ?>
      <img src="<?php print image_style_url('image_showcase_zoom', $image['uri']);?>">
    <?php endforeach; ?>
  <?php endif; ?>
</div>
