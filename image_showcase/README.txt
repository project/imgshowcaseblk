Image Showcase Block

The Image Showcase Block creates a block for images with slider and
options to zoom in node view page.

Administration settings.
-------------------------
After enabling the module in
content type -> manage fields -> edit option you can see an option
'Treat this field as a block for scroll and zoom'. If this option is
enabled then a block will be created in the corresponding field.
You can place the block wherever on the node view page. If this block
is enabled it will convert the images of the node to a block with
slide and zoom.

The theme 'image_showcase_images' returns the images with
slider options and zoom. We need to pass only the images to
the theme and will return the result.

For this module to work you should download and add the following
to the libraries.
1. Slick - https://github.com/kenwheeler/slick/.
    Download the slick and extract it. Inside the folder there will
    be folder named slick copy the folder and place it in libraries.

2. Elevatezoom - https://github.com/elevateweb/elevatezoom.
    Download elevatezoom extract it rename the folder to elevatezoom
    an place it in libraries.
