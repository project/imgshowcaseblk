<?php
/**
 * @file
 * Contains the block functions.
 */

/**
 * Function for theming the images of a node.
 *
 * @return array
 *   renderable array of images with scroll and zoom options.
 */
function image_showcase_node_theme_images($images) {
  $output = theme('image_showcase_images', array('data' => $images));
  return $output;
}
