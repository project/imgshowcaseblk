/**
 * @file
 * Javascript for manae image slides using jQuery.
 */

(function ($) {
  Drupal.image_showcase = Drupal.image_showcase || {};

  /**
   * Attaching events to elements.
   */
  Drupal.behaviors.image_showcase = {
    attach: function(context, settings) {
      $('.image-showcase-zoom__sliderlg').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.image-showcase-zoom__slider'
      });

      $('.image-showcase-zoom__slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.image-showcase-zoom__sliderlg',
        autoplaySpeed: 2000,
        focusOnSelect: true,
      });

      // Elevate Zoom starts here.
      function intElevateZoom(id){
        var parWidth = $(".image-showcase-zoom").width() - 350;
        var parHeight = $(".image-showcase-zoom").height();
        var zoomWindowOffetxVar = $(window).innerWidth() > 1024 ? 44 : 35;
        zoomWindowPosition = 0;
        if (typeof settings.lang != "undefined") {
          var lang_code = settings.lang.lang_code;
          if (lang_code == 'ar') {
            zoomWindowPosition = 11;
          }
        }

        $(id).elevateZoom({
          cursor: "default",
          responsive:true,
          easing:true,
          zoomWindowWidth:parWidth,
          zoomWindowHeight:parHeight,
          zoomWindowFadeIn: 500,
          zoomWindowFadeOut: 500,
          zoomWindowOffetx: zoomWindowOffetxVar,
          zoomWindowPosition: zoomWindowPosition,
          borderSize: 0
        })
      }

      function getCurrentSlide(){
        return $('.image-showcase-zoom__sliderlg').find('.slick-active img');
      }

      $('.image-showcase-zoom__slider').
      on('click', '.slick-slide, .slick-next, .slick-prev', function(slick, currentSlide) {
        if (isMobile()) {
          return;
        }
        var item = getCurrentSlide();
        var id = "zoom_" + Math.floor(Math.random() * (1000 - 10) + 10);
        item.attr({
          id: id
        });
        intElevateZoom('#' + id);
      });

      function isMobile(){
        return $(window).width() < 991;
      }

      function mobileOff(){
        if (isMobile()) {
          $('.image-showcase-zoom__enlarge').hide();
          $('.zoomContainer').remove();
          $('.image-showcase-zoom__sliderlg img').each(function(i, val) {
            $(val).removeData('elevateZoom');
          })
        }
        else {
          $('.image-showcase-zoom__enlarge').show();
          getCurrentSlide().attr({'id': 'zoom2'});
          intElevateZoom('#zoom2');
        }
      }
      getCurrentSlide().attr({'id': 'zoom2'});
      intElevateZoom('#zoom2');
    }
  };
})(jQuery);
